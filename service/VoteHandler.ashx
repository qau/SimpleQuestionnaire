﻿<%@ WebHandler Language="C#" Class="VoteHandler" %>

using System;
using System.Web;
using System.Text;
using System.Collections.Generic;

public class VoteHandler : IHttpHandler {
    HttpContext ctx;
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";


        ctx = context;

        string action = ctx.Request["Action"];

        switch (action)
        {
            case "Visit":
                Visit();
                break;
            case "Get":
                Get();
                break;
            case "Vote":
                Vote();
                break;

            default:
                break;
        }
    }

    public void Visit()
    {
        string voteNo = ctx.Request["voteNo"];
        string openId = ctx.Request["openId"];      

        VoteDAL.Visit(voteNo, openId);
    }
    
    public void Get()
    {
        StringBuilder sbHtml = new StringBuilder();
        string voteNo = ctx.Request["voteNo"];
        string openId = ctx.Request["openId"];
        
       
        VoteModel vm = VoteDAL.Get(voteNo, openId);
        
        sbHtml.Append(HtmlHelper.VoteInfo(vm));

        if (vm.Voted == "已投" || vm.Status == "关闭")
        {
            sbHtml.Append("<div class=\"weui-mask_transparent\"></div>");
        }

        foreach (QuestionModel q in vm.Question)
        {
            if (q.QuestionType == "单选")
            {
                sbHtml.Append(HtmlHelper.RadioList(q));
            }
            else if (q.QuestionType == "多选")
            {
                sbHtml.Append(HtmlHelper.CheckBoxList(q));
            }
            else if (q.QuestionType == "图片单选")
            {
                sbHtml.Append(HtmlHelper.RadioImageList(q));
            }
            else if (q.QuestionType == "文本框")
            {
                sbHtml.Append(HtmlHelper.TextBox(q));
            }
            else if (q.QuestionType == "文本域")
            {
                sbHtml.Append(HtmlHelper.TextArea(q));
            }
        }

        ctx.Response.Write(sbHtml);
    }

    public void Vote()
    {
        string voteNo = ctx.Request["voteNo"];
        string openId = ctx.Request["openId"];
        string ctrlName = "";
        string ctrlValue = "";

        List<VoteResultModel> vrm = new List<VoteResultModel>();
        for (int i = 0; i < ctx.Request.Form.Count; i++)
        {
            ctrlName = ctx.Request.Form.GetKey(i);
            ctrlValue = ctx.Request.Form[i];
            
            if (ctrlName.StartsWith("q"))
            {
                VoteResultModel m = new VoteResultModel();
                m.VoteNo = voteNo;
                m.Votor = openId;
                if (ctrlName.StartsWith("qtext"))
                {
                    if (ctrlName.IndexOf("_") > 0 )
                    {
                        if (!string.IsNullOrEmpty(ctrlValue))
                        {
                            string qo = ctrlName.Substring(5);
                            string[] qId_oId = qo.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);

                            m.QuestionId = qId_oId[0];
                            m.OptionId = qId_oId[1];
                            m.Answer = ctrlValue;
                            m.VoteNo = voteNo;
                            m.Votor = openId;
                            m.CreatedOn = DateTime.Now;


                            VoteResultModel vm = vrm.Find(p => p.QuestionId == m.QuestionId && p.OptionId == m.OptionId);

                            if (vm != null)
                            {
                                vm.Answer = ctrlValue;
                            }
                            else
                            {
                                vrm.Add(m);
                            }
                        }
                    }
                    else
                    {
                        m.QuestionId = ctrlName.Substring(5);
                        m.OptionId = m.QuestionId;
                        m.Answer = ctrlValue;
                        m.VoteNo = voteNo;
                        m.Votor = openId;
                        m.CreatedOn = DateTime.Now;
                        vrm.Add(m);
                    }
                }
                else
                {
                    if (ctrlValue.IndexOf(",") !=-1)
                    {
                        string[] qId_oId = ctrlValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (string oid in qId_oId)
                        {
                            m = new VoteResultModel(); 
                            m.VoteNo = voteNo;
                            m.Votor = openId;
                            m.QuestionId = ctrlName.Substring(1);
                            m.OptionId = oid;
                            m.VoteNo = voteNo;
                            m.Votor = openId;
                            m.CreatedOn = DateTime.Now;
                            vrm.Add(m);
                        }                        
                    }
                    else
                    {
                        m.QuestionId = ctrlName.Substring(1);
                        m.OptionId = ctrlValue;
                        m.VoteNo = voteNo;
                        m.Votor = openId;
                        m.CreatedOn = DateTime.Now;
                        vrm.Add(m);
                    }
                }
            }
        }

        try
        {

            VoteDAL.Save(vrm);
            ctx.Response.Write("提交成功");
        }
        catch (Exception err)
        {
            ctx.Response.Write(err.Message);            
        }
    }
    
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}