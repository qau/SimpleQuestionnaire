﻿<%@ Page Language="C#" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui" />
    <link href="css/weui.css"  rel="stylesheet" />
    <link href="css/questionnaire.css" rel="stylesheet" />
    <title>投票</title>
</head>
<body>
    <form id="form1" class="weui-cells_form" runat="server">        
      <%
          if (Session["openId"] == null)
          {
              Response.End(); 
          } 
          
           string voteNo = Request.QueryString["voteNo"];
           string openId = Session["openId"].ToString();
            

           VoteDAL.Visit(voteNo, openId);       
           VoteModel vm = VoteDAL.Get(voteNo, openId);     
           if (vm.Voted == "已投" || vm.Status=="关闭")
           {
               formSubmitBtn.Disabled = true;
               formSubmitBtn.Attributes.Add("class", "weui-btn weui-btn_primary weui-btn_disabled");
               Response.Write("<div class=\"weui-mask_transparent\"></div>");
           }
      
           
           Response.Write(HtmlHelper.VoteInfo(vm));

           foreach (QuestionModel q in vm.Question)
           {
               if (q.QuestionType == "单选")
               {
                   Response.Write(HtmlHelper.RadioList(q));
               }
               else if (q.QuestionType == "多选")
               {
                   Response.Write(HtmlHelper.CheckBoxList(q));
               }
               else if (q.QuestionType == "图片单选")
               {
                   Response.Write(HtmlHelper.RadioImageList(q));
               }
               else if (q.QuestionType == "文本框")
               {
                   Response.Write(HtmlHelper.TextBox(q));
               }
               else if (q.QuestionType == "文本域")
               {
                   Response.Write(HtmlHelper.TextArea(q));
               }
           }
      %> 
        <input type="hidden" name="voteNo"  value="<%=Request.QueryString["voteNo"] %>" />
        <input type="hidden" name="openId"    value="<%= Session["openId"].ToString() %>" />
    </form>  
     
    <div class="weui-btn-area">
        <button id="formSubmitBtn"  runat="server" class="weui-btn weui-btn_primary">提交</button>
    </div>
    <script src="js/zepto.min.js"></script>
    <script src="js/weui.js"></script>
    <script>
        <%--   
        $().ready( 
            function () {
                $.post("service/VoteHandler.ashx", { "Action": "Get", "voteNo": "<% =Request.QueryString["voteNo"] %>" }, function (data, status, xhr) { $("#qf").append(data); console.log(data); });
            } );--%>

        /* form */
        // 约定正则
        var regexp = {
            regexp: {
                IDNUM: /(?:^\d{15}$)|(?:^\d{18}$)|^\d{17}[\dXx]$/,
                VCODE: /^.{4}$/
            }
        };


        if (!/Android|MicroMessenger|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) {
            $("body").addClass("pg");
        }


        // 失去焦点时检测
        weui.form.checkIfBlur('#form1', regexp);

        // 表单提交
        document.querySelector('#formSubmitBtn').addEventListener('click', function () {

            weui.form.validate('#form1', function (error) {
                //console.log(error);
                if (!error) {
                    var loading = weui.loading('提交中...');

                    //必须有form元素才能序列化
                    //weui.alert($('#form1').serialize());

                    $.ajax({
                        type: 'post',
                        url: 'service/VoteHandler.ashx?Action=Vote',
                        data: $('#form1').serialize(),
                        success: function (data) {
                            setTimeout(function () {
                                loading.hide();
                                weui.toast(data, 3000);
                                $("#formSubmitBtn").attr("disabled", true);
                                $("#formSubmitBtn").attr("class", "weui-btn weui-btn_primary weui-btn_disabled");
                            }, 1500);

                            $.post("service/VoteHandler.ashx", { "Action": "Get", "voteNo": "<% =Request.QueryString["voteNo"] %>", "openId": "<% =Session["openId"].ToString()%>" },
                                function (data, status, xhr) { $("#form1").html(data); });
                        },
                        error: function (xhr, type) {
                            weui.alert(xhr.responseText);
                        }
                    });
                }
            }, regexp);
            return false;
        });
    </script>
</body>
</html>
