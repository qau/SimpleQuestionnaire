--创建数据库
--CREATE DATABASE Questionnaire

--投票/问卷调研主表
CREATE TABLE [dbo].[tbl_vote_conf](
	[本轮投票编号] bigint NOT NULL PRIMARY KEY,
	[本轮投票标题] [varchar](50) NULL,
	[本轮投票描述] [varchar](250) NULL,
	[本轮投票开始时间] [smalldatetime] NULL,
	[本轮投票截止时间] [smalldatetime] NULL,
	[投票启用] [varchar](2) default('是'),
	[投票量]  bigint default(0),
	[访问量] bigint default(0)
)  
--投票/问卷调研明细表
CREATE TABLE [dbo].[tbl_vote_result](
	[本轮投票编号] bigint NULL,
	[问题序号] [int] NULL,
	[问题名称] [varchar](100) NULL,
	[选项序号] [int] NULL,
	[选项描述] [varchar](100) NULL,
	[选项图片] [varchar](250) NULL,
	[投票计数] bigint NULL,
	[题型] [varchar](30) NULL,
	[是否有补充] [varchar](30) NULL,
)  

--投票/问卷调研用户答题表
CREATE TABLE [dbo].[tbl_vote_log](
	[本轮投票编号] bigint NULL,
	[问题序号] [int] NULL,
	[选项序号] [int] NULL,
	[选项值] [varchar](200) NULL,
	[投票人OPENID] [varchar](250) NULL,
	[投票时间] [smalldatetime] NULL
)  

--访问记录
CREATE TABLE [dbo].[tbl_vote_view_log](
	[本轮投票编号] bigint  NULL,
	[投票人OPENID] [varchar](250) NULL,
	[浏览时间] [smalldatetime] NULL
)