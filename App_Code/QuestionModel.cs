﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// QuestionModel 的摘要说明
/// </summary>
public class QuestionModel
{
	public QuestionModel()
	{
		//
		// TODO: 在此处添加构造函数逻辑
		//
        Option = new List<OptionModel>();
	}

    public string VoteNo { get; set; }

    public string QuestionId { get; set; }

    public int SerialNo { get; set; }

    public string QuestionName { get; set; }

    public string QuestionType { get; set; }

    public List<OptionModel> Option { get; set; }

    public string Answer { get; set; }

    public string HasText { get; set; }
}