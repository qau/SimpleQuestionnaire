﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// VoteDAL 的摘要说明
/// </summary>
public class VoteDAL
{

    public static string connString =   System.Configuration.ConfigurationManager.ConnectionStrings["Questionnaire"].ConnectionString;
    public const string SQL_VOTE_SELECT = "SELECT * FROM dbo.tbl_vote_conf WHERE 本轮投票编号 = @VoteNo";

    public const string SQL_VOTE_RESULT_SELECT = "SELECT * FROM tbl_vote_result WHERE 本轮投票编号 = @VoteNo ORDER BY [问题序号],[选项序号]";

    public const string SQL_VOTE_LOG_SELECT = "SELECT * FROM  tbl_vote_log WHERE 本轮投票编号 = @VoteNo AND 投票人OPENID=@OpenId";

    public const string SQL_VOTE_LOG_CREATE = "INSERT INTO tbl_vote_log([本轮投票编号],[问题序号],[选项序号],[选项值],[投票人OPENID],[投票时间]) VALUES(@VoteNo,@QuestionId,@OptionId,@Answer,@OpenId,@CreatedOn)";
    public const string SQL_VOTE_LOG_DELETE = "DELETE FROM tbl_vote_log WHERE 本轮投票编号 = @VoteNo AND 投票人OPENID=@OpenId";

    public const string SQL_VOTE_VIEW_LOG_CREATE = "INSERT INTO tbl_vote_view_log([本轮投票编号],[投票人OPENID],[浏览时间]) VALUES(@VoteNo,@OpenId,GETDATE())";

    public const string SQL_VOTE_ITEM_COUNT_UPDATE = @"UPDATE T
                                                    SET  T.投票计数=ISNULL(S.投票计数,0)
                                                    FROM tbl_vote_result T 
                                                    LEFT JOIN ( SELECT 本轮投票编号,问题序号,选项序号,COUNT(*) 投票计数  FROM dbo.tbl_vote_log
                                                           WHERE  本轮投票编号= @VoteNo 
                                                           GROUP BY 本轮投票编号,问题序号,选项序号) S 
                                                    ON T.本轮投票编号=S.本轮投票编号 AND T.问题序号=S.问题序号 AND T.选项序号=S.选项序号
                                                    WHERE T.本轮投票编号= @VoteNo ";

    public const string SQL_VOTE_COUNT_UPDATE = @"UPDATE T 
                                                    SET T.投票量 =ISNULL(S.投票量,0)
                                                    FROM tbl_vote_conf T 
                                                    JOIN (
                                                    SELECT 本轮投票编号,COUNT(DISTINCT 投票人OPENID) 投票量   FROM tbl_vote_log
                                                    WHERE 本轮投票编号=@VoteNo
                                                    GROUP BY 本轮投票编号 ) S ON T.本轮投票编号=S.本轮投票编号
                                                    WHERE T.本轮投票编号=@VoteNo";

    public const string SQL_VISIT_COUNT_UPDATE = @"UPDATE T 
                                                    SET T.访问量 =ISNULL(S.访问量,0)
                                                    FROM tbl_vote_conf T 
                                                    JOIN (
                                                    SELECT 本轮投票编号,COUNT( 投票人OPENID) 访问量   FROM tbl_vote_view_log
                                                    WHERE 本轮投票编号=@VoteNo
                                                    GROUP BY 本轮投票编号 ) S ON T.本轮投票编号=S.本轮投票编号
                                                    WHERE T.本轮投票编号=@VoteNo";


    public const string SQL_VISIT_COUNT_INC_UPDATE = @"UPDATE tbl_vote_conf  SET  访问量 =ISNULL(访问量,0) + 1   WHERE  本轮投票编号=@VoteNo";




    public static VoteModel Get(string voteNo, string openId)
    {
        VoteModel voteM = new VoteModel();
        voteM.VoteNo = voteNo;
        voteM.OpenId = openId;
        DataTable dtAnwser = GetAnwser(voteNo, openId);
        voteM.Voted = "未投";
        if (dtAnwser != null && dtAnwser.Rows.Count > 0)
        {
            voteM.Voted = "已投";
        }
        using (SqlConnection conn = new SqlConnection(connString))
        {

            if (conn.State != ConnectionState.Open)
                conn.Open();

            SqlCommand cmd = new SqlCommand(SQL_VOTE_SELECT, conn);
            cmd.Parameters.Add("@VoteNo", voteNo);

            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                voteM.VoteNo = reader["本轮投票编号"].ToString();
                voteM.VoteTitle = reader["本轮投票标题"].ToString();
                voteM.VoteDesc = reader["本轮投票描述"].ToString();
                voteM.BeginDate = Convert.ToDateTime(reader["本轮投票开始时间"]);
                voteM.ExpireDate = Convert.ToDateTime(reader["本轮投票截止时间"]);

                if (!Convert.IsDBNull(reader["投票启用"]) && reader["投票启用"].ToString() == "是"
                    && voteM.BeginDate < DateTime.Now && voteM.ExpireDate > DateTime.Now)
                    voteM.Status = "开放";
                else
                    voteM.Status = "关闭";

                voteM.VoteCount = Convert.ToInt16(reader["投票量"]);
                voteM.VisitCount = Convert.ToInt16(reader["访问量"]);
            }
            reader.Close();


            cmd.CommandText = SQL_VOTE_RESULT_SELECT;
            reader = cmd.ExecuteReader();

            string questionId = "";
            QuestionModel q = null;
            while (reader.Read())
            {
                if (questionId != reader["问题序号"].ToString())
                {
                    q = new QuestionModel();
                    questionId = reader["问题序号"].ToString();
                    q.VoteNo = voteNo;
                    q.QuestionId = questionId;
                    q.SerialNo = Convert.ToInt16(questionId);
                    q.QuestionName = reader["问题名称"].ToString();
                    q.QuestionType = reader["题型"].ToString();
                    q.HasText = reader["是否有补充"].ToString();

                    voteM.Question.Add(q);
                }

                OptionModel option = new OptionModel();

                option.OptionId = reader["选项序号"].ToString();
                option.OptionName = reader["选项描述"].ToString();
                if (!Convert.IsDBNull(reader["选项图片"]))
                {
                    option.OptionImage = reader["选项图片"].ToString();
                }

                if (!Convert.IsDBNull(reader["投票计数"]))
                {
                    option.VoteCount = Convert.ToInt16(reader["投票计数"]);

                    if (voteM.VoteCount != 0)
                        option.VoteRate = Math.Round(option.VoteCount * 1.0m / voteM.VoteCount * 1.0m, 4, MidpointRounding.AwayFromZero);
                }
                if (dtAnwser != null && dtAnwser.Rows.Count > 0)
                {

                    DataRow drAnwser = dtAnwser.Rows.Find(new object[] { reader["问题序号"], reader["选项序号"] });

                    if (drAnwser != null)
                    {
                        option.Checked = "checked";


                        if (!Convert.IsDBNull(drAnwser["选项值"]) && !string.IsNullOrEmpty(drAnwser["选项值"].ToString()))
                        {
                            q.Answer = drAnwser["选项值"].ToString();
                        }
                    }
                }

                q.Option.Add(option);
            }

            reader.Close();
        }

        return voteM;
    }

    public bool IsVote(string voteNo, string openId)
    {
        bool result = false;
        using (SqlConnection conn = new SqlConnection(connString))
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();

            SqlCommand cmd = new SqlCommand(SQL_VOTE_LOG_SELECT, conn);
            cmd.Parameters.Add("@VoteNo", voteNo);
            cmd.Parameters.Add("@OpenId", openId);

            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                result = true;
            }
        }
        return result;
    }


    public static DataTable GetAnwser(string voteNo, string openId)
    {
        DataTable dtAnwser = new DataTable();
        using (SqlConnection conn = new SqlConnection(connString))
        {

            if (conn.State != ConnectionState.Open)
                conn.Open();

            SqlCommand cmd = new SqlCommand(SQL_VOTE_LOG_SELECT, conn);
            cmd.Parameters.Add("@VoteNo", voteNo);
            cmd.Parameters.Add("@OpenId", openId);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);

            adapter.Fill(dtAnwser);

            dtAnwser.PrimaryKey = new DataColumn[] { dtAnwser.Columns["问题序号"], dtAnwser.Columns["选项序号"] };
        }

        return dtAnwser;
    }

    public static void Visit(string voteNo, string openId)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();

            SqlCommand cmd = new SqlCommand(SQL_VOTE_VIEW_LOG_CREATE, conn);
            cmd.Parameters.Add("@VoteNo", SqlDbType.BigInt, 30).Value = voteNo;
            cmd.Parameters.Add("@OpenId", SqlDbType.VarChar, 200).Value = openId;
            cmd.ExecuteNonQuery();

            cmd.Parameters.Clear();
            cmd.CommandText = SQL_VISIT_COUNT_INC_UPDATE;
            cmd.Parameters.Add("@VoteNo", SqlDbType.BigInt, 30).Value = voteNo;
            cmd.ExecuteNonQuery();
        }
    }

    public static void Save(List<VoteResultModel> vrm)
    {
        string voteNo = vrm[0].VoteNo;
        using (SqlConnection conn = new SqlConnection(connString))
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            try
            {
                SqlCommand deleteCmd = new SqlCommand(SQL_VOTE_LOG_DELETE, conn);
                deleteCmd.Parameters.Add("@VoteNo", SqlDbType.BigInt, 30).Value = vrm[0].VoteNo;
                deleteCmd.Parameters.Add("@OpenId", SqlDbType.VarChar, 200).Value = vrm[0].Votor;

                deleteCmd.Transaction = tran;
                deleteCmd.ExecuteNonQuery();

                SqlCommand insertCmd = new SqlCommand(SQL_VOTE_LOG_CREATE, conn);
                insertCmd.Transaction = tran;
                insertCmd.Parameters.Add("@VoteNo", SqlDbType.BigInt, 30);
                insertCmd.Parameters.Add("@QuestionId", SqlDbType.Int);
                insertCmd.Parameters.Add("@OptionId", SqlDbType.Int);
                insertCmd.Parameters.Add("@Answer", SqlDbType.VarChar, 200);
                insertCmd.Parameters.Add("@OpenId", SqlDbType.VarChar, 200);
                insertCmd.Parameters.Add("@CreatedOn", SqlDbType.DateTime);

                foreach (VoteResultModel m in vrm)
                {
                    insertCmd.Parameters[0].Value = m.VoteNo;
                    insertCmd.Parameters[1].Value = m.QuestionId;
                    insertCmd.Parameters[2].Value = m.OptionId;
                    insertCmd.Parameters[3].Value = string.IsNullOrEmpty(m.Answer) ? "" : m.Answer;
                    insertCmd.Parameters[4].Value = m.Votor;
                    insertCmd.Parameters[5].Value = m.CreatedOn;

                    insertCmd.ExecuteNonQuery();
                }

                tran.Commit();
            }
            catch (Exception err)
            {
                if (tran != null)
                    tran.Rollback();

                throw err;
            }
        }

        Stats(voteNo);
    }



    public static void Stats(string voteNo)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            try
            {
                SqlCommand voteItemStatsCmd = new SqlCommand(SQL_VOTE_ITEM_COUNT_UPDATE, conn);
                voteItemStatsCmd.Transaction = tran;
                voteItemStatsCmd.Parameters.Add("@VoteNo", SqlDbType.BigInt, 30).Value = voteNo;
                voteItemStatsCmd.ExecuteNonQuery();

                SqlCommand voteStatsCmd = new SqlCommand(SQL_VOTE_COUNT_UPDATE, conn);
                voteStatsCmd.Transaction = tran;
                voteStatsCmd.Parameters.Add("@VoteNo", SqlDbType.BigInt, 30).Value = voteNo;
                voteStatsCmd.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception err)
            {
                if (tran != null)
                    tran.Rollback();

                throw err;
            }
        }
    }
}