﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// VoteResultModel 的摘要说明
/// </summary>
public class VoteResultModel
{

    public string VoteNo { get; set; }

    public string QuestionId { get; set; }

    public string OptionId { get; set; }

    public string Answer { get; set; }

    public string Votor { get; set; }

    public DateTime CreatedOn { get; set; }
}