﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// VoteModel 的摘要说明
/// </summary>
public class VoteModel
{
    public VoteModel()
    {
        Question = new List<QuestionModel>();
    }

    public string VoteNo { get; set; }

    public string VoteTitle { get; set; }

    public string VoteDesc { get; set; }

    public DateTime BeginDate { get; set; }

    public DateTime ExpireDate { get; set; }

    public string Status { get; set; }

    public string Voted { get; set; }

    public string OpenId { get; set; }

    public int VoteCount { get; set; }

    public int VisitCount { get; set; }

    public List<QuestionModel> Question { get; set; }
}