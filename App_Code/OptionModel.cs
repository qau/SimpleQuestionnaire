﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// OptionModel 的摘要说明
/// </summary>
public class OptionModel
{
	public OptionModel()
	{
		//
		// TODO: 在此处添加构造函数逻辑
		//
	}

    public string OptionId { get; set; }

    public string OptionName { get; set; }

    public string OptionImage { get; set; }

    public string Checked { get; set; }

    public int VoteCount { get;set; }

    public decimal VoteRate { get; set; }
}