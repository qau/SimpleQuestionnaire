﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

/// <summary>
/// HtmlHelper 的摘要说明
/// </summary>
public class HtmlHelper
{

    public static StringBuilder VoteInfo(VoteModel m)
    {
        StringBuilder sbHtml = new StringBuilder();

        sbHtml.AppendFormat(@" <div class='weui-cells vote-info'>
            <div class='weui-cell'>
                <div class='weui-cell__hd'>投票编号: </div>
                <div class='weui-cell__bd'>{0}</div>
            </div>
             <div class='weui-cell'>
                <div class='weui-cell__hd'>投 票 量: </div>
                <div class='weui-cell__bd'>{1}</div>
            </div>
             <div class='weui-cell'>
                <div class='weui-cell__hd'>访 问 量: </div>
                <div class='weui-cell__bd'>{2}</div>
            </div>
             <div class='weui-cell'>
                <div class='weui-cell__hd'>开始时间: </div>
                <div class='weui-cell__bd'>{3}</div>
            </div>
             <div class='weui-cell'>
                <div class='weui-cell__hd'>截止时间: </div>
                <div class='weui-cell__bd'>{4}</div>
            </div>
             <div class='weui-cell'>
                <div class='weui-cell__hd'>投票状态: </div>
                <div class='weui-cell__bd'>{5}</div>
            </div>
             <div class='weui-cell'>
                <div class='weui-cell__hd'>用户编号: </div>
                <div class='weui-cell__bd'>{6}</div>
            </div>
             <div class='weui-cell'>
                <div class='weui-cell__hd'>用户状态: </div>
                <div class='weui-cell__bd'>{7}</div>
            </div>
             <div class='weui-cell'>
                <div class='weui-cell__hd'>投票描述: </div>
                <div class='weui-cell__bd'>{8}</div>
            </div>
        </div>", m.VoteNo, m.VoteCount, m.VisitCount, m.BeginDate, m.ExpireDate, m.Status,m.OpenId,m.Voted, m.VoteDesc);

        return sbHtml;
    }


    public static StringBuilder RadioList(QuestionModel m)
    {
        StringBuilder sbHtml = new StringBuilder();
        sbHtml.AppendFormat(" <div class=\"weui-cells__title\">{0}、{1}</div>", m.QuestionId, m.QuestionName);
        sbHtml.Append(@"<div class='weui-cells weui-cells_radio'>");
        int index = 0;
        foreach (OptionModel option in m.Option)
        {
            if (index == 0)
            {
                sbHtml.AppendFormat(@"<label class='weui-cell weui-check__label' for='q{0}_{1}'>
                    <div class='weui-cell__hd'>
                        <input type='radio' class='weui-check' name='q{0}' id='q{0}_{1}' value='{1}' {4} required tips='{0}、{3},请勾选1个' />
                        <span class='weui-icon-checked'></span>
                    </div>
                    <div class='weui-cell__bd'>
                       {2}
                    </div>
                </label> ", m.QuestionId, option.OptionId, option.OptionName, m.QuestionName, option.Checked);
            }
            else
            {
                sbHtml.AppendFormat(@"<label class='weui-cell weui-check__label' for='q{0}_{1}'>
                    <div class='weui-cell__hd'>
                        <input type='radio' class='weui-check' name='q{0}' id='q{0}_{1}' value='{1}' {4} />
                        <span class='weui-icon-checked'></span>
                    </div>
                    <div class='weui-cell__bd'>
                       {2}
                    </div>
                </label> ", m.QuestionId, option.OptionId, option.OptionName, m.QuestionName,option.Checked);

                if (index == m.Option.Count - 1 && !string.IsNullOrEmpty(m.HasText) && m.HasText == "是")
                {
                    sbHtml.AppendFormat(@"<div class='weui-cell'>
                            <div class='weui-cell__bd'>
                                <input type='text' class='weui-input' placeholder='请输其它' name='qtext{0}_{1}' value='{2}'  />
                            </div>
                            </div>", m.QuestionId,index+1,m.Answer);
                }
            }
            index++;
        }
        sbHtml.Append("</div>");

        return sbHtml;
    }

    public static StringBuilder CheckBoxList(QuestionModel m)
    {
        StringBuilder sbHtml = new StringBuilder();
        sbHtml.AppendFormat(" <div class=\"weui-cells__title\">{0}、{1}</div>", m.QuestionId, m.QuestionName);
        sbHtml.Append(@" <div class='weui-cells weui-cells_checkbox'>");
        int index = 0;
        foreach (OptionModel option in m.Option)
        {
            if (index == 0)
            {
                sbHtml.AppendFormat(@" <label class='weui-cell weui-check__label' for='q{0}_{1}'>
                    <div class='weui-cell__hd'>
                        <input type='checkbox' class='weui-check' name='q{0}' value='{1}' {4} id='q{0}_{1}' required  pattern='{{1,}}' tips='{0}、{3},请之少勾选1个' />
                        <i class='weui-icon-checked'></i>
                    </div>
                    <div class='weui-cell__bd'>
                     {2}
                    </div>
                </label>", m.QuestionId, option.OptionId, option.OptionName, m.QuestionName, option.Checked);
            }
            else
            {
                sbHtml.AppendFormat(@" <label class='weui-cell weui-check__label' for='q{0}_{1}'>
                    <div class='weui-cell__hd'>
                        <input type='checkbox' class='weui-check' name='q{0}' value='{1}' {4}  id='q{0}_{1}'  />
                        <i class='weui-icon-checked'></i>
                    </div>
                    <div class='weui-cell__bd'>
                     {2}
                    </div>
                </label>", m.QuestionId, option.OptionId, option.OptionName, m.QuestionName, option.Checked);

                if (index == m.Option.Count - 1 && !string.IsNullOrEmpty(m.HasText) && m.HasText == "是")
                {
                    sbHtml.AppendFormat(@"<div class='weui-cell'>
                            <div class='weui-cell__bd'>
                                <input type='text' class='weui-input' placeholder='请输其它' name='qtext{0}_{1}' value='{2}'  />
                            </div>
                            </div>", m.QuestionId,index+1,m.Answer);
                }
            }
            index++;
        }
        sbHtml.Append("</div>");
        return sbHtml;
    }


    public static StringBuilder RadioImageList(QuestionModel m)
    {
        StringBuilder sbHtml = new StringBuilder();
        sbHtml.AppendFormat("<div class='weui-cells__title'>{0}、{1}</div>", m.QuestionId, m.QuestionName);
        sbHtml.Append("<div class='weui-cells weui-cells_radio'>");
        foreach (OptionModel option in m.Option)
        {
            sbHtml.Append("<div class='radioimage'>");
            sbHtml.AppendFormat(@"<label class='weui-cell weui-check__label' for='q{0}_{1}'>
                    <div class='weui-cell__hd'>
                        <input type='radio' name='q{0}' class='weui-check' value='{1}' {4} id='q{0}_{1}' />
                        <span class='weui-icon-checked'></span>
                    </div>
                    <div class='weui-cell__bd'>
                        <p>{1}、{2}</p>
                    </div>
                </label>
                <label class='weui-cell' for='q{0}_{1}'>
                    <div class='weui-cell__bd'>
                        <img src='{3}'  style='width:100%'>
                    </div>
                </label>", m.QuestionId, option.OptionId, option.OptionName, option.OptionImage, option.Checked);


            sbHtml.AppendFormat(@"<div class='weui-cell'>
                    <div class='weui-cell__bd'>
                      支持率: {0:##.##}%   投票数量:{1}
                    </div>
                </div>", option.VoteRate * 100, option.VoteCount);
            sbHtml.Append("</div>");
        }
        sbHtml.Append("</div>");

        return sbHtml;
    }

    public static StringBuilder TextBox(QuestionModel m)
    {
        StringBuilder sbHtml = new StringBuilder();

        sbHtml.AppendFormat(@"<div class='weui-cells__title'>{0}</div>
            <div class='weui-cell'>
                <div class='weui-cell__bd'>
                    <input type='text' class='weui-input' placeholder='请输{0}' name='qtext{1}' value='{2}' required />
                </div>
                <div class='weui-cell__ft'><i class='weui-icon-warn'></i> </div>
            </div>",m.QuestionName,m.QuestionId,m.Answer);
        return sbHtml;
    }

    public static StringBuilder TextArea(QuestionModel m)
    {
        StringBuilder sbHtml = new StringBuilder();

        sbHtml.AppendFormat(@"<div class='weui-cells__title'>{0}</div>
          <div class='weui-cells weui-cells_form'>
            <div class='weui-cell'>
                <div class='weui-cell__bd'>
                    <textarea class='weui-textarea' placeholder='请输{0}' name='qtext{1}' rows='3'  required> {2}</textarea>
                </div>
                <div class='weui-cell__ft'><i class='weui-icon-warn'></i> </div>
            </div>
            </div>", m.QuestionName, m.QuestionId,m.Answer);
        return sbHtml;
    }
}