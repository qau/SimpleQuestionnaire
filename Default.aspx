﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>投票/问卷调研系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/weui.css"  rel="stylesheet" />
</head>
<body>

    <div class="weui-cells__title">当期投票/问卷调研</div>
    <asp:Literal runat="server" ID="currentperiod"></asp:Literal>
  
    <div class="weui-cells__title">往期投票/问卷调研</div>
    <asp:Literal runat="server" ID="passedperiod"></asp:Literal>
  
</body>
</html>
