﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default : System.Web.UI.Page
{
    const string SQL_VOTE_SELECT = "SELECT 本轮投票编号,本轮投票标题,CASE WHEN 本轮投票截止时间>GETDATE() THEN '1' ELSE '0' END 是否过期 FROM  tbl_vote_conf ORDER BY  本轮投票开始时间  DESC";

    //原始跳转
    static string FMT_ITEM = "<div class=\"weui-cell\"><div class=\"weui-cell__bd\"> <a  href=\"Questionnaire.aspx?voteNo={0}\">{1}</a></div></div>";


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            MakeClient();
            DataTable dtVote = new DataTable();
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["Questionnaire"].ConnectionString;

            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(SQL_VOTE_SELECT, conn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dtVote);
                }
            }

            StringBuilder sbCurrentPeriod = new StringBuilder();
            StringBuilder sbPassedPeriod = new StringBuilder();

            foreach (DataRow dr in dtVote.Rows)
            {
                if (dr["是否过期"] != DBNull.Value && dr["是否过期"].ToString() == "1")
                {
                    sbCurrentPeriod.AppendFormat(FMT_ITEM, dr["本轮投票编号"], dr["本轮投票标题"]);
                }
                else
                {
                    sbPassedPeriod.AppendFormat(FMT_ITEM, dr["本轮投票编号"], dr["本轮投票标题"]);
                }
            }
            currentperiod.Text = sbCurrentPeriod.ToString();
            passedperiod.Text = sbPassedPeriod.ToString();
        }
        catch (Exception err)
        {
            currentperiod.Text = "出错了," + err.Message;
        }
    }

    //没有集成到微信里面暂时用cookie代替
    void MakeClient()
    {
        HttpCookie cookie = Request.Cookies["openId"];

        if (null == cookie)
        {
            string openId = Guid.NewGuid().ToString("N");
            Session["openId"] = openId;
            cookie = new HttpCookie("openId");
            cookie.Values["openId"] = openId;
            cookie.Expires = DateTime.Now.AddYears(100);
            Response.Cookies.Add(cookie);
        }
        else
        {
            cookie.Expires = DateTime.Now.AddYears(100);
            Session["openId"] = cookie.Values["openId"] ;
        }
    }
}